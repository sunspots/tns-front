import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import index from '@/view/index'
import info from '@/view/info'
import message from '@/view/message'
import navlist from '@/view/navlist'
import chatroom from '@/view/chatroom'
import onlineresume from '@/view/onlineresume'
import setaccount from '@/view/setaccount'
import firminfopublish from '@/view/firminfopublish'
import recruitlist from '@/view/recruitlist'
import firminfo from '@/view/firminfo'
import hr from '@/view/hr'
import firm from '@/view/firm'
import details from '@/view/details'
import recruitdetails from '@/view/recruitdetails'
import applymanager from '@/view/applymanager'
import infotanlenters from '@/view/infotanlenters'
import account from '@/view/account'
import personage from '@/view/personage'




Vue.use(Router)

export default new Router({


	routes: [{
			path: '/',
			name: 'HelloWorld',
			component: HelloWorld
		},
		{
			path: '/index',
			name: 'index',
			component: index,
		},
		{
			path: '/navlist',
			name: 'navlist',
			component: navlist
		},
		{
			path: '/info',
			name: 'info',
			component: info
		},
		
		{
			path: '/message',
			name: 'message',
			component: message,
			children:[{
				path:'/chatroom',
				name:'chatroom',
				component:chatroom
			}]
		},
		{
			path: '/onlineresume',
			name: 'onlineresume',
			component: onlineresume
		},
		{
			path: '/setaccount',
			name: 'setaccount',
			component: setaccount
		},
		{
			path: '/firminfopublish',
			name: 'firminfopublish',
			component: firminfopublish
		},
		{
			path: '/recruitlist',
			name: 'recruitlist',
			component: recruitlist
		},
		{
			path: '/firminfo',
			name: 'firminfo',
			component: firminfo
		},
		{
			path: '/hr',
			name: 'hr',
			component: hr
		},
		{
			path: '/firm',
			name: 'firm',
			component: firm
		},
		{
			path: '/details',
			name: 'details',
			component: details
		},
		{
			path: '/recruitdetails',
			name: 'recruitdetails',
			component: recruitdetails
		},
		{
			path: '/applymanager',
			name: 'applymanager',
			component: applymanager
		},
		{
			path: '/infotanlenters',
			name: 'infotanlenters',
			component: infotanlenters
		},
		{
			path: '/account',
			name: 'account',
			component: account
		},{
			path: '/personage',
			name: 'personage',
			component: personage
		},
	]
})