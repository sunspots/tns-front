import Vuex from 'vuex';
import Vue from 'vue';
Vue.use(Vuex);
const store = new Vuex.Store({
	state: {
		//设置全局访问的state对象
		loginstate:true,
	},
	getters: {
		//实时监听state值得变化
	},
	mutations: {
		//自定义改变state初始值的方法，参数除了state之外，还可以有其他的参数（变量或对象）
		setloginstate(state,loginstate){
			state.loginstate = loginstate;
		}
	},
	actions: {

	}
})
export default store;